Implementation of Actor-Critic for Decentralized Control algorithm presented in <sup>[1](#coopmapg)</sup>

<a name="coopmapg">[1]</a> Cooperative Multi-Agent Policy Gradient
\- Guillaume Bono, Jilles Steeve Dibangoye, Laëtitia Matignon, Florian Pereyron, Olivier Simonin

European Conference on Machine Learning (2018)

url: http://www.ecmlpkdd2018.org/wp-content/uploads/2018/09/518.pdf
