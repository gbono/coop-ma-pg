#!/usr/bin/env python3

import os
from argparse import ArgumentParser

import torch

from utils import ProgressBar, ResultsExporter
from .reinforce_loss import ReinforceLoss


HORIZON = 10
TRIALS = 10
EPISODES = 1000
BATCH = 1
LR = 0.001
LR_DECAY = 0.96
DISP_STEP = 10


class BaseReinforceAlgorithm:
    def __init__(self, algo = "reinforce"):
        self.algo = algo

        self.parser = ArgumentParser(description = "Train network using reinforce algorithm or variant")
        self.parser.add_argument("filename", type = str,
                help = "File describing problem to be parsed by model")
        self.parser.add_argument("--discount", "-d", type = float, default = None,
                help = "Discount on future reward (overwrite value parsed from file)")
        self.parser.add_argument("--horizon", "-p", type = int, default = HORIZON,
                help = "Planning horizon")
        self.parser.add_argument("--trials", "-t", type = int, default = TRIALS,
                help = "Number of trials to run")
        self.parser.add_argument("--episodes", "-e", type = int, default = EPISODES,
                help = "Number of training episodes per trial")
        self.parser.add_argument("--batch", "-b", type = int, default = BATCH,
                help = "Size of minibatch in each episode")
        self.parser.add_argument("--learning-rate", "-l", type = float, default = LR,
                help = "Learning rate applied to update parameters")
        self.parser.add_argument("--rate-decay", "-y", type = float, default = LR_DECAY,
                help = "Decay applied to learning rate")
        self.parser.add_argument("--rate-step", "-s", type = int, default = None,
                help = "Stepsize to decrease learning rate by RATE_DECAY")
        self.parser.add_argument("--clip-grad", "-c", type = float, default = None,
                help = "Clip gradients to given value")
        self.parser.add_argument("--error-type", type = str,
                choices = ["mc", "td"], default = "mc",
                help = "Error type used in REINFORCE loss (Monte-Carlo or Time-Difference)")
        self.parser.add_argument("--output", "-o", type = str, default = "default",
                help = "Prefix of basename for files containing episodes' values \
                        averaged over trials, intermediate and final weights")
        self.parser.add_argument("--export-loss", action = "store_true",
                help = "Generate an other csv file with episodes' loss averaged over trials")
        self.parser.add_argument("--no-cuda", dest = "cuda", action = "store_false",
                help = "Disable cuda backend")

    def init_environment(self):
        raise NotImplementedError()

    def init_model(self):
        raise NotImplementedError()

    def reset(self):
        raise NotImplementedError()

    def step(self):
        raise NotImplementedError()

    def add_argument(self, *args, **kwargs):
        self.parser.add_argument(*args, **kwargs)

    def get_args(self, args = None):
        self.args = self.parser.parse_args(args)
        if torch.cuda.is_available() and self.args.cuda:
            self.args.device = torch.device("cuda")
        else:
            self.args.device = torch.device("cpu")
        return self.args

    def run(self):
        args = self.get_args()

        self.env = self.init_environment()
        self.env.to(device=args.device)

        if args.discount:
            self.env.discount = args.discount

        bench = os.path.basename(args.filename)
        bench, _ = os.path.splitext(bench)
        out_prefix = os.path.join("results", self.algo, bench)
        exporter = ResultsExporter( os.path.join( out_prefix, "{}_ep_value.csv".format(args.output) ) )
        if args.export_loss:
            loss_exporter = ResultsExporter( os.path.join( out_prefix, "{}_ep_loss.csv".format(args.output) ) )

        weights = []

        for tr in range(1, args.trials+1):
            self.model = self.init_model().to(device=args.device)
            criterion = ReinforceLoss(self.env.discount, self.env.has_reward, args.error_type).to(device=args.device)
            optimizer = torch.optim.SGD(self.model.parameters(), lr = args.learning_rate)
            if args.rate_step is not None:
                optim_sched = torch.optim.lr_scheduler.StepLR(optimizer, args.rate_step, args.rate_decay)

            progress = ProgressBar(args.episodes)
            buf_emp = 0
            buf_est = 0
            buf_loss = 0

            exporter.start_trial()
            if args.export_loss:
                loss_exporter.start_trial()

            for ep in range(1, args.episodes+1):
                self.reset()

                r_seq = []
                logp_seq = []
                val_seq = []

                for t in range(args.horizon):
                    r, logp, val = self.step()

                    r_seq.append(r)
                    logp_seq.append(logp)
                    val_seq.append(val)

                r_seq = torch.stack(r_seq)
                logp_seq = torch.stack(logp_seq)
                val_seq = None if val_seq[0] is None else torch.stack(val_seq)

                loss = criterion(r_seq, logp_seq, val_seq)
                optimizer.zero_grad()
                loss.backward()
                if args.clip_grad is not None:
                    for p in self.model.parameters():
                        p.grad.data.clamp_(-args.clip_grad, args.clip_grad)
                optimizer.step()
                if args.rate_step is not None:
                    optim_sched.step()

                r_cumul = sum( r * self.env.discount**t for t,r in enumerate(r_seq) )
                buf_emp += r_cumul.mean()
                if val_seq is not None:
                    buf_est += val_seq[0].data.mean()
                buf_loss += loss.item()

                if val_seq is None:
                    exporter.update( r_cumul.mean().item() )
                else:
                    exporter.update( val_seq[0].mean().item() )
                if args.export_loss:
                    loss_exporter.update( loss.item() )

                if ep % DISP_STEP == 0:
                    last_val = buf_emp / DISP_STEP if val_seq is None else buf_est / DISP_STEP
                    last_loss = buf_loss / DISP_STEP
                    if val_seq is None:
                        progress.update(ep, emp = buf_emp / DISP_STEP, loss = buf_loss / DISP_STEP)
                    else:
                        progress.update(ep, emp = buf_emp / DISP_STEP, est = buf_est / DISP_STEP, loss = buf_loss / DISP_STEP)
                    print(progress, end = '')

                    buf_emp  = 0
                    buf_est  = 0
                    buf_loss = 0

            print("\nFinished trial #{}".format(tr))
            self.model.to(torch.device("cpu"))
            weights.append( ( last_val, abs(last_loss), self.model.state_dict() ) )

        best = max(weights, key = lambda t : t[0:2])[-1]
        out_prefix = os.path.join("weights", self.algo, bench)
        if not os.path.isdir(out_prefix):
            os.makedirs(out_prefix)
        out_path = os.path.join(out_prefix, "{}_ep{}_best.pyth".format(args.output, args.episodes) )
        print("Writing weights to '{}'...".format(out_path), end = '')
        torch.save( best, out_path)
        print(" Done!")

        exporter.save()
        if args.export_loss:
            loss_exporter.save()
