import torch
import torch.nn as nn

class ActorCritic(nn.Module):
    def __init__(self, feature_module, policy_module,
            feature_size = 1024, bias = False):
        super().__init__()
        self.add_module("features", feature_module)
        self.add_module("actor", policy_module)
        self.critic = nn.Linear(feature_size, 1)

    def init_hiddens(self, batch_size):
        return self.features.init_hiddens(batch_size)

    def init_inputs(self, batch_size):
        return self.features.init_inputs(batch_size)

    def forward(self, *args):
        feats_out = self.features(*args)
        a, logp = self.actor(feats_out[0])
        val = self.critic(feats_out[0])
        return (a, logp, val, *feats_out[1:])
