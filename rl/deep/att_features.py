import torch
import torch.nn as nn
import torch.nn.functional as F

from rl.deep import Attention
from math import sqrt


class AttentionFeatures(nn.Module):
    """
    Feature extraction based on Recurrent Neural Network with Attention
    providing Phi(a_1, z_1 ... a_t, z_t)
    """
    NO_INPUT = -1

    def __init__(self, input_size, output_size, horizon, embedding_size = 128,
            hidden_size = 128, bias = False):
        """
        Constructor.
        Args:
            - input_size     [int]  : Size of discrete input space
            - output_size    [int]  : Size of output features
            - horizon        [int]  : Planning horizon limiting attention dim
            - embedding_size [int]  : Kernel/Embedding size to encode observations
            - hidden_size    [int]  : Hidden and cell states size
            - bias           [bool] : Enable bias parameter for all layers
        """
        super().__init__()

        self.emb = nn.Embedding(1 + input_size, embedding_size)
        self.att = Attention(embedding_size, hidden_size, embedding_size, horizon)
        self.rnn = nn.LSTMCell(embedding_size, hidden_size, bias = bias)
        self.fc = nn.Linear(hidden_size, output_size, bias = bias)

        self.h0 = nn.Parameter( torch.FloatTensor(1, hidden_size) )
        self.c0 = nn.Parameter( torch.FloatTensor(1, hidden_size) )
        self.reset_parameters()

        self.register_buffer( "i0", torch.LongTensor(1,1).fill_(AttentionFeatures.NO_INPUT) )
        self.register_buffer( "seq0", torch.zeros(1, horizon, embedding_size) )

    def reset_parameters(self):
        """
        Initialize hidden parameters.
        """
        stdv = 1.0 / sqrt( self.h0.numel() )
        with torch.no_grad():
            self.h0.uniform_(-stdv, stdv)
            self.c0.uniform_(-stdv, stdv)

    def init_hiddens(self, batch_size = 1):
        """
        Get initial hidden parameters.
        Args:
            - batch_size [int]                          : Size of batch
        Returns:
            - hiddens    [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        return self.h0.expand(batch_size, -1), self.c0.expand(batch_size, -1)

    def init_inputs(self, batch_size = 1):
        """
        Get initial (empty) inputs.
        Args:
            - batch_size [int]                      : Size of batch
        Returns:
            - empty_in  [LongTensor  N x 1]         : Batch of initial (empty) inputs
            - empty_seq [FloatTensor N x 1 x D_EMB] : Batch of empty embeddings
        """
        return self.i0.expand(batch_size, -1), self.seq0.expand(batch_size, -1, -1)

    def forward(self, i, seq, hiddens):
        """
        Update internal hidden state and get internal features.
        Args:
            - i       [LongTensor  N x 1]            : Batch of inputs
            - seq     [FloatTensor N x t x D_EMB]    : Batch of sequences of embeddings
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        Returns:
            - feats   [FloatTensor N x |A|]          : Batch of features
            - x       [FloatTensor N x D_EMB]        : Batch of embeddings
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        x = self.emb(i + 1).squeeze(1)
        x_att = self.att(x, hiddens[0], seq)
        h,c = self.rnn(x_att, hiddens)
        feats = F.relu( self.fc(h) )
        return feats, x, (h,c)
