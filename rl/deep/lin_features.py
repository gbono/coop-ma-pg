import torch.nn as nn
import torch.nn.functional as F


class LinearFeatures(nn.Module):
    def __init__(self, input_size, output_size, bias = False):
        """
        Constructor.
        Args:
            - input_size     [int]  : Size of discrete input space
            - output_size    [int]  : Size of output features
            - bias           [bool] : Enable bias parameter for all layers
        """
        super().__init__()
        self.fc = nn.Linear(input_size, output_size, bias = bias)

    def forward(self, i):
        """
        Compute features.
        Args:
            - i       [LongTensor  N x 1]   : Batch of inputs
        Returns:
            - feats   [FloatTensor N x |A|] : Batch of features
        """
        return F.relu( self.fc(i) )
