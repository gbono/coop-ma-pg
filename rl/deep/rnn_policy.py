import torch.nn as nn

from rl.deep import RNNFeatures, LinearPolicy


class RNNPolicy(nn.Module):
    """
    Stochastic policy based on Recurrent Neural Network
    providing actions a_t ~ pi(a | z_0 ... z_{t-1})
    """
    def __init__(self, input_size, output_size, embedding_size = 128,
            hidden_size = 128, feature_size = 256, bias = False):
        """
        Constructor.
        Args:
            - input_size     [int]  : Size of discrete input space
            - output_size    [int]  : Size of discrete output space
            - embedding_size [int]  : Kernel/Embedding size to encode observations
            - hidden_size    [int]  : Hidden and cell states size
            - feature_size   [int]  : Number of features extracted from hidden state
            - bias           [bool] : Enable bias parameter for all layers
        """
        super().__init__()

        self.features = RNNFeatures(input_size, feature_size, embedding_size,
                hidden_size, bias)
        self.policy = LinearPolicy(feature_size, output_size)

    def init_hiddens(self, batch_size = 1):
        """
        Get initial hidden parameters.
        Args:
            - batch_size [int]                          : Size of batch
        Returns:
            - hiddens    [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        return self.features.init_hiddens(batch_size)

    def init_inputs(self, batch_size = 1):
        """
        Get initial (empty) inputs.
        Args:
            - batch_size [int]              : Size of batch
        Returns:
            - empty_obs  [LongTensor N x 1] : Batch of initial (empty) observations
        """
        return self.features.init_inputs(batch_size)

    def forward(self, i, hiddens):
        """
        Forward pass: update internal hidden state, compute features, sample action.
        Args:
            - i       [LongTensor N x 1]             : Batch of inputs
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        Returns:
            - a       [LongTensor  N x 1]            : Batch of action ids
            - logp    [FloatTensor N x 1]            : Batch of corresponding log probabilities
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        feats, hiddens = self.features(i, hiddens)
        a, logp = self.policy(feats)
        return a, logp, hiddens
