import torch
import torch.nn as nn
import torch.nn.functional as F

from rl.deep import AttentionFeatures, LinearPolicy
from math import sqrt


class AttentionPolicy(nn.Module):
    """
    Stochastic policy based on Recurrent Neural Network with Attention
    providing actions a_t ~ pi(a | z_0 ... z_{t-1})
    """
    def __init__(self, input_size, output_size, horizon, embedding_size = 128,
            hidden_size = 128, feature_size = 256, bias = False):
        """
        Constructor.
        Args:
            - input_size     [int]  : Size of discrete input space
            - output_size    [int]  : Size of discrete output space
            - horizon        [int]  : Planning horizon limiting attention dim
            - embedding_size [int]  : Kernel/Embedding size to encode observations
            - hidden_size    [int]  : Hidden and cell states size
            - feature_size   [int]  : Number of features extracted from hidden state
            - bias           [bool] : Enable bias parameter for all layers
        """
        super().__init__()

        self.features = AttentionFeatures(input_size, feature_size, horizon, embedding_size,
                hidden_size, bias)
        self.policy = LinearPolicy(feature_size, output_size, bias)

    def init_hiddens(self, batch_size = 1):
        """
        Get initial hidden parameters.
        Args:
            - batch_size [int]                                    : Size of batch
        Returns:
            - hiddens    [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        return self.features.init_hiddens(batch_size)

    def init_inputs(self, batch_size = 1):
        """
        Get initial (empty) inputs.
        Args:
            - batch_size [int]                      : Size of batch
        Returns:
            - empty_in  [LongTensor  N x 1]         : Batch of initial (empty) inputs
            - empty_seq [FloatTensor N x 1 x D_EMB] : Batch of empty embeddings
        """
        return self.features.init_inputs(batch_size)

    def forward(self, i, seq, hiddens):
        """
        Forward pass: update internal hidden state, compute features, sample action.
        Args:
            - i       [LongTensor  N x 1]            : Batch of inputs
            - seq     [FloatTensor N x t x D_EMB]    : Batch of sequences of embeddings
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        Returns:
            - a       [LongTensor  N x 1]            : Batch of action ids
            - logp    [FloatTensor N x 1]            : Batch of corresponding log probabilities
            - x       [FloatTensor N x D_EMB]        : Batch of embeddings
            - hiddens [tuple of FloatTensor N x D_H] : Hidden and cell states
        """
        feats, x, hiddens = self.features(i, seq, hiddens)
        a, logp = self.policy(feats)
        return a, logp, x, hiddens
