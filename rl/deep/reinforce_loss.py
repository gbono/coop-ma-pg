import torch
import torch.nn as nn
import torch.nn.functional as F


class ReinforceLoss(nn.Module):
    """
    Module implementing REINFORCE loss.
    """
    def __init__(self, discount = 1.0, use_reward = True, err_type = "mc",
            size_average = True, reduce = True):
        """
        Constructor.
        Args:
            - discount     [float]        : Discount factor applied to future rewards
            - use_reward   [bool]         : If set to False, minimize cost instead of maximizing reward
            - err_type     ["mc" or "td"] : Switch between Monte-Carlo and Time-Difference error
            - size_average [bool]         : If set to False, reduce minibatch using sum instead of average
            - reduce       [bool]         : If set to false, keep one value per sample in minibatch
        """
        super().__init__()
        self.register_buffer( "discount", torch.FloatTensor([discount]) )
        self.use_reward = use_reward
        self.use_td = (err_type == "td")
        self.size_average = size_average
        self.reduce = reduce

    def forward(self, r_seq, logp_seq, val_seq = None):
        """
        Forward pass.
        Args:
            - r_seq    [FloatTensor T x N x 1] : Sequence of batch of rewards or costs
            - logp_seq [FloatTensor T x N x 1] : Sequence of batch of log probabilities
            - val_seq  [FloatTensor T x N x 1] : Sequence of batch of estimated values
                    (or None if no critic)
        """
        if val_seq is None:
            disc_seq = self.discount.view(1,1,1).expand_as(r_seq).cumprod(dim = 0)
            adv_seq = r_seq * disc_seq
            critic_loss = 0
        else:
            if self.use_td:
                target = r_seq[:-1] + self.discount * val_seq.data[1:]
                target = torch.cat([target, r_seq[-1].unsqueeze(0)], dim = 0)
            else:
                r_cumul = r_seq[-1]
                target = [r_cumul]
                for t in range(-2,-r_seq.size(0)-1,-1):
                    r_cumul = r_seq[t] + self.discount * r_cumul
                    target.append(r_cumul)
                target.reverse()
                target = torch.stack(target)
            critic_loss = ( val_seq - target )**2
            adv_seq = (target - val_seq).detach()

        if self.use_reward:
            loss = (-logp_seq * adv_seq + critic_loss).sum(dim = 0)
        else:
            loss = ( logp_seq * adv_seq + critic_loss).sum(dim = 0)

        if not self.reduce:
            return loss
        return loss.mean() if self.size_average else loss.sum()
