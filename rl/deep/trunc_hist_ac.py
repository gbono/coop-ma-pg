from collections import deque
import torch
import torch.nn as nn
import torch.nn.functional as F

class TruncatedHistoryActorCritic(nn.Module):
    def __init__(self, n_observations, n_actions, horizon, memory = 1):
        super().__init__()
        self.n_obs = n_observations
        self.n_act = n_actions
        self.mem = memory
        self.hrz = horizon
        self.actor  = nn.Embedding((1 + n_observations * n_actions) ** memory, n_actions)
        self.critic = nn.Embedding((1 + n_observations * n_actions) ** memory, n_actions)
        self.act_history = deque(maxlen = memory)
        self.obs_history = deque(maxlen = memory)
        self.register_buffer("empty_history",
                torch.LongTensor(1,1).zero_())

    def reset_history(self):
        self.act_history.clear()
        self.obs_history.clear()

    def forward(self, new_observation = None, batch_size = 1):
        """
        Args:
            - new_observation [LongTensor  N x 1]
            - batch_size      [uint]
        Returns:
            - new_action      [LongTensor  N x 1]
            - logprob         [FloatTensor N x 1]
        """
        if new_observation is None:
            indices = self.empty_history.expand(batch_size, -1)
        else:
            indices = self.empty_history.expand_as(new_observation)
            self.obs_history.append(new_observation)
            for a,z in zip(self.act_history, self.obs_history):
                indices = (self.n_act * self.n_obs + 1) * indices + (self.n_act * z + a + 1)

        features = self.actor(indices).squeeze(1)
        new_action = F.softmax(features, dim = 1).multinomial(1)
        self.act_history.append(new_action)

        logprob = F.log_softmax(features, dim = 1).gather(1, new_action)
        values = self.critic(indices).squeeze(1).gather(1, new_action)
        return new_action, logprob, values
