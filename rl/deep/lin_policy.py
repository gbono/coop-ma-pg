import torch.nn as nn
import torch.nn.functional as F

class LinearPolicy(nn.Module):
    def __init__(self, input_size, output_size, bias = False):
        super().__init__()
        self.fc = nn.Linear(input_size, output_size, bias = bias)

    def forward(self, feats):
        """
        Args:
            - feats   [FloatTensor N x D_F]
        Returns:
            - a       [LongTensor  N x 1]
            - logp    [FloatTensor N x 1]
        """
        scr = self.fc(feats)                               #.size() = N x |A|
        probs = F.softmax(scr, dim = 1)                    #.size() = N x |A|
        logprobs = F.log_softmax(scr, dim = 1)             #.size() = N x |A|
        a = probs.multinomial(1)                           #.size() = N x 1
        logp = logprobs.gather(1, a)                       #.size() = N x 1
        entropy = -(logprobs * probs).sum(dim = -1).mean() #.size() = 1
        return a, logp
