import torch
import torch.nn as nn
import torch.nn.functional as F


class Attention(nn.Module):
    def __init__(self, embedding_size, hidden_size, target_size, target_length):
        """
        Args:
            - embedding_size = D_EMB
            - hidden_size    = D_H
            - target_size    = D_I
            - target_length  = T
        """
        super().__init__()
        self.seq_len = target_length

        self.score = nn.Linear(embedding_size + hidden_size, target_length, bias = False)
        self.combine = nn.Linear(target_size + embedding_size, embedding_size, bias = False)

    def forward(self, x, h, seq):
        """
        Args:
            - x   [N x D_EMB]
            - h   [N x D_H]
            - seq [N x t x D_I]
        Returns:
            - att [N x D_EMB]
        """
        seq = F.pad(seq, (0,0,0,self.seq_len - seq.size(1)) )
        scr = F.softmax(self.score( torch.cat([x, h], dim = 1) ), dim = 1)
        att = scr.unsqueeze(1).bmm(seq).squeeze(1)
        combo = F.relu( self.combine( torch.cat([att, x], dim = 1) ) )
        return combo
