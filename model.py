import torch
import torch.nn as nn

from .parser_helpers import *


class DecPOMDP(nn.Module):
    """
    Class representing an instance of Decentralized Partially Observable Markov Decision Process
    and providing a 'reset()', 'execute()' interface to sample minibatch of trajectories.
    (Public) Attributes:
        - agents                [list of Agent] : List of agents (see DecPOMDP.Agent)
        - n_agents              [int]           : Number of agents (abbrev. |Ag|)
        - discount              [float]         : Discount factor applied to future rewards
        - has_reward / has_cost [bool]          : Type of optimization described (max reward or min cost)
        - states                [list of str]   : List of unobservable/internal states
        - n_states              [int]           : Number of internal states (abbrev. |S|)
        - n_joint_actions       [int]           : Number of joint actions (abbrev. |JA|)
        - n_joint_observations  [int]           : Number of joint observations (abbrev. |JZ|)
        - start                 [torch.FloatTensor (|S|)] :
                Initial state probability distribution
        - transition_mat        [torch.FloatTensor (|JA| x |S| x |S|)] :
                Probability of transitionning from state s (dim 1) to state s' (dim 2)
                when executing joint action ja (dim 0)
        - observation_mat       [torch.FloatTensor (|JA| x |S| x |JZ|)] :
                Probability of observing jz (dim 2) after executing joint action ja (dim 0)
                and landing in state s' (dim 1)
        - reward_mat            [torch.FloatTensor (|JA| x |S| x |S| x |JZ|)] :
                Reward jointly obtained by the agents when experiencing
                transition from state s (dim 1) to state s' (dim 2)
                executing joint action ja (dim 0) and observing jz (dim 3)
    """
    class Agent:
        """
        Internal class storing all parameters for a single agent
        (Public) Attributes:
            - name           [str]
            - actions        [list of str]
            - n_actions      [int]
            - observations   [list of str]
            - n_observations [int]
        """
        def __init__(self, name, actions = [], observations = []):
            self.name = name
            self.actions = actions
            self.n_actions = len(actions)
            self.observations = observations
            self.n_observations = len(observations)

    @staticmethod
    def parse(filename):
        """
        Generate DecPOMDP instance from text file in the .dpomdp format.
        """
        raw_data = read_file(filename)
        ag_names = read_count_or_enum(raw_data["agents"][0], "ag")
        ag_actions = read_items(raw_data["actions"][0], [name + "_a" for name in ag_names])
        ag_observations = read_items(raw_data["observations"][0], [name + "_z" for name in ag_names])
        states = read_count_or_enum(raw_data["states"][0], "s")
        return DecPOMDP(
                agents = [DecPOMDP.Agent(name, a, z) for name, a, z in zip(ag_names, ag_actions, ag_observations)],
                discount = float(raw_data["discount"][0]),
                val_type = read_field(raw_data["values"][0])[0],
                states = states,
                start = read_start(raw_data, states),
                transition_mat = read_transition(raw_data["T"], states, ag_actions),
                observation_mat = read_observation(raw_data["O"], states, ag_actions, ag_observations),
                reward_mat = read_reward(raw_data["R"], states, ag_actions, ag_observations)
                )

    @staticmethod
    def load(filename):
        """
        Load a DecPOMDP instance from a .pyth file previously saved.
        """
        return torch.load(filename)

    def save(self, filename):
        """
        Save as a .pyth binary file for faster future instanciations.
        """
        torch.save(self, filename)

    def __init__(self, agents = [], discount = 1.0, val_type = "reward",
            states = [], start = None, transition_mat = None,
            observation_mat = None, reward_mat = None):
        """
        Constructor.
        """
        super().__init__()

        self.agents = agents
        self.n_agents = len(agents)
        self.discount = discount
        self.has_reward = (val_type == "reward")
        self.has_cost = (val_type == "cost")
        self.states = states
        self.n_states = len(states)
        self.n_joint_actions = prod([ag.n_actions for ag in self.agents])
        self.n_joint_observations = prod([ag.n_observations for ag in self.agents])

        self.register_buffer("start",
                torch.ones(self.n_states) / self.n_states if start is None else start)
        self.register_buffer("transition_mat",
                torch.ones(self.n_joint_actions, self.n_states, self.n_states) \
                / self.n_states if transition_mat is None else transition_mat)
        self.register_buffer("observation_mat",
                torch.ones(self.n_joint_actions, self.n_states, self.n_joint_observations) \
                / self.n_joint_observations if observation_mat is None else observation_mat)
        self.register_buffer("reward_mat",
                torch.ones(self.n_joint_actions, self.n_states, self.n_states, self.n_joint_observations) \
                * -1.0 if reward_mat is None else reward_mat)

        m, M = self.reward_mat.min(), self.reward_mat.max()
        scaled = ( 2*self.reward_mat - (M + m) ) / (M - m)
        self.register_buffer("scaled_reward", scaled)
        self.scaled = False

    def __str__(self):
        """
        Format readable string for this DecPOMDP instance.
        """
        return "agents: " + ' '.join([ag.name for ag in self.agents]) + '\n' \
                + "discount: {}\n".format(self.discount) \
                + "values: {}\n".format("reward" if self.has_reward else "cost") \
                + "states: " + ' '.join(self.states) + '\n' \
                + "start:\n" \
                + ' '.join(str(p) for p in self.start) + '\n' \
                + "actions:\n" \
                + '\n'.join(' '.join(ag.actions) for ag in self.agents) + '\n' \
                + "observations:\n" \
                + '\n'.join(' '.join(ag.observations) for ag in self.agents) + '\n' \
                + "T: Tensor of size {}\n".format("x".join(str(d) for d in self.transition_mat.size())) \
                + "O: Tensor of size {}\n".format("x".join(str(d) for d in self.observation_mat.size())) \
                + "R: Tensor of size {}\n".format("x".join(str(d) for d in self.reward_mat.size()))

    def get_formated_joint_action(self, joint_action_id):
        """
        Format a readable string for the joint action corresponding to a given index.
        """
        indiv_ids = self.get_indiv_action_ids(joint_action_id)
        return ", ".join( ag.actions[a] for ag,a in zip(self.agents, indiv_ids) )

    def get_formated_joint_observation(self, joint_observation_id):
        """
        Format a readable string for the joint observation corresponding to a given index.
        """
        indiv_ids = self.get_indiv_observation_ids(joint_observation_id)
        return ", ".join( ag.observations[z] for ag,z in zip(self.agents, indiv_ids) )

    def get_joint_action_id(self, action_ids):
        """
        Compute the unique joint action index corresponding to the given individual actions' indices.
        Args:
            - action_ids [list of torch.LongTensor N] : List of (batch of) individual action(s)
                        OR [list of int]
        Returns:
            - joint_id   [torch.LongTensor N]         : (Batch of) joint action(s)
                        OR [int]
        """
        joint_id = action_ids[0]
        for ag, a_id in zip(self.agents[1:], action_ids[1:]):
            joint_id = ag.n_actions * joint_id + a_id
        return joint_id

    def get_joint_observation_id(self, observation_ids):
        """
        Compute the unique joint observation index corresponding to the given individual observations' indices.
        Args:
            - observation_ids [list of torch.LongTensor N] : List of (batch of) individual observation(s)
                            OR [list of int]
        Returns:
            - joint_id        [torch.LongTensor N]         : (Batch of) joint observation(s)
                            OR [int]
        """
        joint_id = observation_ids[0]
        for ag, o_id in zip( self.agents[1:], observation_ids[1:]):
            joint_id = ag.n_observations * joint_id + o_id
        return joint_id

    def get_indiv_action_ids(self, joint_action_id):
        """
        Compute the individual actions' indices corresponding to the given joint action index.
        Args:
            - joint_action_id [torch.LongTensor N] : (Batch of) joint action(s)
                            OR [int]
        Returns:
            - indiv_ids       [list of torch.LongTensor N] : List of (batch of) individual action(s)
                            OR [list of int]
        """
        ja = joint_action_id
        indiv_ids = []
        for ag in reversed(self.agents):
            if hasattr(ja, "remainder"):
                ja, a = ja.div(ag.n_actions), ja.remainder(ag.n_actions)
            else:
                ja, a = divmod(ja, ag.n_actions)
            indiv_ids.append(a)
        indiv_ids.reverse()
        return indiv_ids

    def get_indiv_observation_ids(self, joint_observation_id):
        """
        Compute the individual observations' indices corresponding to the given joint observation index.
        Args:
            - joint_observation_id [torch.LongTensor N]         : (Batch of) joint observation(s)
                                OR [int]
        Returns:
            - indiv_ids            [list of torch.LongTensor N] : List of (batch of) individual observation(s)
                                OR [list of int]
        """
        jz = joint_observation_id
        indiv_ids = []
        for ag in reversed(self.agents):
            if hasattr(jz, "remainder"):
                jz, z = jz.div(ag.n_observations), jz.remainder(ag.n_observations)
            else:
                jz, z = divmod(jz, ag.n_observations)
            indiv_ids.append(z)
        indiv_ids.reverse()
        return indiv_ids

    def reset(self, batch_size = 1):
        """
        Initialize internal state and belief.
        Args:
            - batch_size [int] : Size of batch
        """
        self.internal = self.start.multinomial(batch_size, True)
        self.belief = self.start.new(batch_size, self.n_states).fill_(1 / self.n_states)

    def get_current_state(self):
        """
        Get current state index.
        Returns:
            - internal [LongTensor N x 1] : Batch of internal (unobservable) states.
        """
        return self.internal.unsqueeze(-1)

    def get_current_belief(self):
        """
        Get current belief per state.
        Returns:
            - belief [FloatTensor N x |S|] : Batch of states' belief
        """
        return self.belief

    def execute(self, joint_action_id):
        """
        Execute joint action, updating internal state and belief, and returning joint observation and reward.
        Args:
            - joint_action_id [LongTensor  N x 1] : Batch of join actions
        Returns:
            - jz              [LongTensor  N x 1] : Batch of joint observations
            - r               [FloatTensor N x 1] : Batch of rewards
        """
        ja = joint_action_id.squeeze(-1)

        nxt = self.transition_mat[ja, self.internal].multinomial(1).squeeze(-1)
        jz = self.observation_mat[ja, nxt].multinomial(1).squeeze(-1)
        r = self.reward_mat[ja, self.internal, nxt, jz]

        self.internal = nxt
        self.belief = self.observation_mat.permute(0,2,1)[ja, jz, :] \
                * ( self.transition_mat[ja, :, :] * self.belief.unsqueeze(-1) ).sum(dim = 1)
        self.belief /= self.belief.sum(dim = 1, keepdim = True)

        return jz.unsqueeze(-1), r.unsqueeze(-1)

    def use_scaled_reward(self, t = True):
        """
        Normalize reward in [-1,1] or restore original values.
        """
        if self.scaled != t:
            self.reward_mat, self.scaled_reward = self.scaled_reward, self.reward_mat
            self.scaled = t
