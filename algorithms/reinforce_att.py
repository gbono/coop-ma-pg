#!/usr/bin/env python3

import torch
import torch.nn.functional as F

from rl.deep import BaseReinforceAlgorithm, AttentionPolicy
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# AttentionPolicy = visualize_nn(AttentionPolicy)


class ReinforceAttention(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            return DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            return DecPOMDP.load(self.args.filename)

    def init_model(self):
        return AttentionPolicy(self.env.n_joint_observations * self.env.n_joint_actions,
                self.env.n_joint_actions, self.args.horizon, feature_size = self.args.features)

    def reset(self):
        self.env.reset(self.args.batch)
        self.persist = ( *self.model.init_inputs(self.args.batch),
                self.model.init_hiddens(self.args.batch) )
        self.x_seq = []

    def step(self):
        a, logp, x, hiddens = self.model(*self.persist)
        a = a.detach()
        z, r = self.env.execute(a)
        self.x_seq.append(x)
        self.persist = (self.env.n_joint_actions * z + a,
                torch.stack(self.x_seq, dim = 1), hiddens)
        return r, logp, None


if __name__ == "__main__":
    algo = ReinforceAttention("reinforce-att")
    algo.add_argument("--features", "-f", type = int, default = 256,
            help = "Number of features")
    algo.run()
