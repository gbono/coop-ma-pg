#!/usr/bin/env python3

from itertools import repeat

import torch
import torch.nn as nn

from rl.deep import BaseReinforceAlgorithm, ActorCritic, RNNFeatures, LinearPolicy
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# @visualize_nn
class MultiAC(nn.Module):
    def __init__(self, agents, features):
        super().__init__()
        for ag in agents:
            phi = RNNFeatures(ag.n_observations * ag.n_actions, features, 64, 64)
            pi = LinearPolicy(features, ag.n_actions)
            self.add_module( "ac_{}".format(ag.name),
                    ActorCritic(phi, pi, features) )

    def init_hiddens(self, batch_size):
        return map( ActorCritic.init_hiddens, self.children(), repeat(batch_size) )

    def init_inputs(self, batch_size):
        return map( ActorCritic.init_inputs, self.children(), repeat(batch_size) )

    def forward(self, i, hiddens):
        return map(ActorCritic.forward, self.children(), i, hiddens)


class DecActorCriticActObs(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            env = DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            env = DecPOMDP.load(self.args.filename)
        #env.use_scaled_reward(True)
        return env

    def init_model(self):
        return MultiAC(self.env.agents, self.args.features)

    def reset(self):
        self.env.reset(self.args.batch)
        self.persist = ( self.model.init_inputs(self.args.batch),
                self.model.init_hiddens(self.args.batch) )

    def step(self):
        a, logp, val, hiddens = zip( *self.model(*self.persist) )
        jz, r = self.env.execute( self.env.get_joint_action_id([ia.detach() for ia in a]) )
        z = self.env.get_indiv_observation_ids(jz)
        self.persist = ([ag.n_actions * iz + ia.detach() \
                for ag,iz,ia in zip(self.env.agents, z, a)], hiddens)
        return r, torch.stack(logp).sum(dim = 0), torch.stack(val).sum(dim = 0)


if __name__ == "__main__":
    algo = DecActorCriticActObs("dec-actor-critic-act-obs")
    algo.add_argument("--features", "-f", type = int, default = 128,
            help = "Number of features per agent")
    algo.run()
