#!/usr/bin/env python3

from rl.deep import BaseReinforceAlgorithm, TruncatedHistoryActorCritic
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# ActorCritic = visualize_nn(ActorCritic)

class ActorCriticTrunc(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            return DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            return DecPOMDP.load(self.args.filename)

    def init_model(self):
        return TruncatedHistoryActorCritic(self.env.n_joint_observations, self.env.n_joint_actions, self.args.memory)

    def reset(self):
        self.env.reset(self.args.batch)
        self.model.reset_history()
        self.persist = (None, self.args.batch)

    def step(self):
        a, logp, val = self.model(*self.persist)
        z, r = self.env.execute( a.detach() )
        self.persist = (z,)
        return r, logp, val


if __name__ == "__main__":
    algo = ActorCriticTrunc("actor-critic-trunc")
    algo.add_argument("--features", "-f", type = int, default = 256,
            help = "Number of features")
    algo.add_argument("--memory", "-m", type = int, default = 1,
            help = "Memory size for truncated histories")
    algo.run()
