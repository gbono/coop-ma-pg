#!/usr/bin/env python3

from rl.deep import BaseReinforceAlgorithm, RNNPolicy
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# RNNPolicy = visualize_nn(RNNPolicy)

class ReinforceActObs(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            return DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            return DecPOMDP.load(self.args.filename)

    def init_model(self):
        return RNNPolicy(self.env.n_joint_observations * self.env.n_joint_actions, self.env.n_joint_actions,
                feature_size = self.args.features)

    def reset(self):
        self.env.reset(self.args.batch)
        self.persist = ( self.model.init_inputs(self.args.batch),
                self.model.init_hiddens(self.args.batch) )

    def step(self):
        a, logp, hiddens = self.model(*self.persist)
        a = a.detach()
        z, r = self.env.execute(a)
        self.persist = (self.env.n_joint_actions * z + a, hiddens)
        return r, logp, None


if __name__ == "__main__":
    algo = ReinforceActObs("reinforce-act-obs")
    algo.add_argument("--features", "-f", type = int, default = 256,
            help = "Number of features")
    algo.run()
