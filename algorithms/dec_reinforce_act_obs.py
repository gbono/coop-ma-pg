#!/usr/bin/env python3

from itertools import repeat

import torch
import torch.nn as nn

from rl.deep import BaseReinforceAlgorithm, RNNPolicy
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# @visualize_nn
class MultiPolicy(nn.Module):
    def __init__(self, agents, features):
        super().__init__()
        for ag in agents:
            self.add_module( "pi_{}".format(ag.name),
                    RNNPolicy(ag.n_observations * ag.n_actions, ag.n_actions, 64, 64, feature_size = features) )

    def init_hiddens(self, batch_size):
        return map( RNNPolicy.init_hiddens, self.children(), repeat(batch_size) )

    def init_inputs(self, batch_size):
        return map( RNNPolicy.init_inputs, self.children(), repeat(batch_size) )

    def forward(self, i, hiddens):
        return map(RNNPolicy.forward, self.children(), i, hiddens)


class DecReinforceActObs(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            return DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            return DecPOMDP.load(self.args.filename)

    def init_model(self):
        return MultiPolicy(self.env.agents, self.args.features)

    def reset(self):
        self.env.reset(self.args.batch)
        self.persist = ( self.model.init_inputs(self.args.batch),
                self.model.init_hiddens(self.args.batch) )

    def step(self):
        a, logp, hiddens = zip( *self.model(*self.persist) )
        jz, r = self.env.execute( self.env.get_joint_action_id([ia.detach() for ia in a]) )
        self.persist = ([ag.n_actions * iz + ia.detach() \
                for ag,iz,ia in zip(self.env.agents, self.env.get_indiv_observation_ids(jz), a)], hiddens)
        return r, torch.stack(logp).sum(dim = 0), None


if __name__ == "__main__":
    algo = DecReinforceActObs("dec-reinforce-act-obs")
    algo.add_argument("--features", "-f", type = int, default = 128,
            help = "Number of features per agent")
    algo.run()
