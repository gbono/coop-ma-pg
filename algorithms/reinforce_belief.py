#!/usr/bin/env python3

import torch.nn as nn
import torch.nn.functional as F

from rl.deep import BaseReinforceAlgorithm, LinearFeatures, LinearPolicy
from decpomdp import DecPOMDP

# # WITH VIZ
# from utils import visualize_nn
# @visualize_nn
class LinFeatPolicy(nn.Module):
    def __init__(self, input_size, output_size, feature_size = 64, bias = False):
        super().__init__()
        self.features = LinearFeatures(input_size, feature_size, bias)
        self.policy = LinearPolicy(feature_size, output_size, bias)

    def forward(self, i):
        feats = self.features(i)
        a, logp = self.policy(feats)
        return a, logp


class ReinforceBelief(BaseReinforceAlgorithm):
    def init_environment(self):
        if self.args.filename.endswith(".dpomdp"):
            return DecPOMDP.parse(self.args.filename)
        elif self.args.filename.endswith(".pyth"):
            return DecPOMDP.load(self.args.filename)

    def init_model(self):
        return LinFeatPolicy(self.env.n_states, self.env.n_joint_actions, self.args.features)

    def reset(self):
        self.env.reset(self.args.batch)

    def step(self):
        b = self.env.get_current_belief()
        a, logp = self.model(b)
        z, r = self.env.execute( a.detach() )
        return r, logp, None


if __name__ == "__main__":
    algo = ReinforceBelief("reinforce-belief")
    algo.add_argument("--features", "-f", type = int, default = 256,
            help = "Number of features")
    algo.run()
