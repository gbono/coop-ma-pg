from decpomdp import DecPOMDP
from glob import glob

for filename in glob("problems/*"):
    if "DISABLED" in filename:
        continue
    print("Loading: {}".format(filename))
    model = DecPOMDP.parse(filename)
    out = filename.replace("problems/","preload/").replace(".dpomdp",".pyth")
    print("Saving to: {}".format(out))
    model.save(out)
