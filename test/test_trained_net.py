#!/usr/bin/env python3

from decpomdp import DecPOMDP
from algorithms.actor_critic_act_obs import *
from algorithms.dec_actor_critic_act_obs import *
from algorithms.dec_actor_critic_att import *
from algorithms.dec_reinforce_act_obs import *
from algorithms.reinforce_act_obs import *
from algorithms.reinforce_att import *
from algorithms.reinforce_belief import *
from algorithms.reinforce_obs import *

import torch
import torch.nn as nn

import sys
import os.path
from matplotlib import pyplot
from scipy.misc import imread


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise SystemExit()

    w_path = sys.argv[1]
    _, algoname, bench, param = w_path.split('/')

    horizon = int(param.split('_', 1)[0][1:])

    algo = eval(''.join(algoname.title().split('-')))()

    algo.get_args()
    algo.args.filename = "preload/{}.pyth".format(bench)
    algo.args.discount = 1
    algo.args.horizon = horizon

    algo.model = algo.init_model()
    algo.policy = algo.init_policy()
    algo.policy.load_state_dict(torch.load(w_path))

    out_prefix = "images/{}/{}/p{}_".format(algoname, bench, horizon)

    algo.policy.visualize_parameters(out_prefix + "weights")

    algo.reset()
    r_seq = []
    for t in range(horizon):
        r, logp, val = algo.step()
        algo.policy.visualize_activations(out_prefix + "activ{}".format(t))
        r_seq.append(r)

    print("Cumulated reward = ", torch.stack(r_seq).sum())

    prob_layers = {
            "reinforce-belief"        : ["001_out"],
            "reinforce-obs"           : ["004_policy.fc"],
            "reinforce-act-obs"       : ["004_policy.fc"],
            "reinforce-att"           : ["004_policy.fc"],
            "actor-critic-act-obs"    : ["004_actor.fc"],
            "dec-reinforce-act-obs"   : ["{:03}_pi_{}.policy.fc".format(i, ag.name) for i,ag in zip(range(4,5*algo.model.n_agents,5), algo.model.agents)],
            "dec-actor-critic-act-obs": ["{:03}_ac_{}.actor.fc".format(i, ag.name) for i,ag in zip(range(4,6*algo.model.n_agents,6), algo.model.agents)],
            "dec-actor-critic-att"    : ["{:03}_ac_{}.actor.fc".format(i, ag.name) for i,ag in zip(range(4,6*algo.model.n_agents,6), algo.model.agents)]
    }

    layer = prob_layers[algoname]

    f, ax = pyplot.subplots(horizon, len(layer), sharex = True, squeeze = False)
    f.suptitle(algoname + '\n' + bench.upper())
    if len(layer) == algo.model.n_agents:
        for i,ag in enumerate(algo.model.agents):
            ax[0,i].set_title(ag.name)
    for t, a in enumerate(ax[:, 0]):
        a.set_ylabel(str(t))
    for t in range(horizon):
        for l,name in enumerate(layer):
            img = imread(out_prefix + "activ{}".format(t) + "/" + name + ".png")
            ax[t,l].set_yticks([])
            ax[t,l].imshow(img)
    if len(layer) == algo.model.n_agents:
        for i,ag in enumerate(algo.model.agents):
            ax[-1,i].set_xticks(list(range(ag.n_actions)))
            ax[-1,i].set_xticklabels(ag.actions)
    else:
        jact = list(range(algo.model.n_joint_actions))
        ax[-1,0].set_xticks(jact)
        ax[-1,0].set_xticklabels([algo.model.get_formated_joint_action(ja).replace(', ', '\n') for ja in jact])
    pyplot.show()
