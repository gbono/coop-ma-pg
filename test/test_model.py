#!/usr/bin/env python3
#-*-coding: utf8-*-

from model import DecPOMDP
import sys

import torch

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print( "Usage: {} [.dpomdp file]".format(sys.argv[0]) )
        raise SystemExit(1)

    dpomdp = DecPOMDP.parse(sys.argv[1])

    print("Test parsing:".center(64))
    print('-'*64)

    print(dpomdp)

    print("\n\n\n")

    print("Test joint <-> indiv conversions".center(64))
    print('-'*64)

    actions0 = torch.cat([torch.LongTensor([a]).repeat(dpomdp.agents[1].n_actions) for a in range(dpomdp.agents[0].n_actions)])
    actions1 = torch.arange(dpomdp.agents[1].n_actions).repeat(dpomdp.agents[0].n_actions).long()
    jactions = dpomdp.get_joint_action_id([actions0, actions1])

    back0, back1 = dpomdp.get_indiv_action_ids(jactions)
    for a0, a1, ja, b0, b1 in zip(actions0, actions1, jactions, back0, back1):
        print([a0, a1], "  -joint->  ", ja, "  -indiv->  ", [b0, b1])

    print("\n\n\n")

    print("Test transitions".center(64))
    print('-'*64)

    for a0, a1, ja, in zip(actions0, actions1, jactions):
        print(dpomdp.agents[0].actions[a0], dpomdp.agents[1].actions[a1])
        T = dpomdp.transition_mat[ja]
        print("s \ s'".center(16) + "|" + " ".join([s.center(16) for s in dpomdp.states]))
        print('-'*(16*(dpomdp.n_states+1)))
        for i,s in enumerate(dpomdp.states):
            print(s.center(16), end = "|")
            for j,ns in enumerate(dpomdp.states):
                print(format(T[i,j], "0.2f").center(16), end = " ")
            print()
        print()

    print("\n\n\n")


    print("Test sampling".center(64))
    print('-'*64)

    batch_str = lambda batch: " ".join(map(lambda i: str(i[0]),batch))

    dpomdp.reset(3)
    for t in range(20):
        s = dpomdp.get_current_state()
        ja = ( torch.rand(3,1) * dpomdp.n_joint_actions ).long()
        jz, r = dpomdp.execute(ja)
        print( "T = {:02d}, S = {}, JA = {}, JZ = {}, R = {}".format(t, batch_str(s), batch_str(ja), batch_str(jz), batch_str(r) ) )
