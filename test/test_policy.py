#!/usr/bin/env python3

from argparse import ArgumentParser

import torch
from torch.autograd import Variable

from decpomdp import DecPOMDP
from rl.deep import RNNPolicy

HORIZON = 10
BATCH = 10

def get_args():
    parser = ArgumentParser()
    parser.add_argument("filename", type = str)
    parser.add_argument("--horizon", "-p", type = int, default = HORIZON)
    parser.add_argument("--batch", "-b", type = int, default = BATCH)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()

    dpomdp = DecPOMDP.parse(args.filename)

    print('-'*64)
    print("Test constructor".center(64))
    print('-'*64)
    policy = RNNPolicy(dpomdp.n_joint_observations, dpomdp.n_joint_actions)
    print(policy)
    print()

    dpomdp.reset(args.batch)
    jz = policy.init_inputs(args.batch)
    hiddens = policy.init_hiddens(args.batch)

    print('-'*64)
    print("Test forward".center(64))
    print('-'*64)

    for t in range(args.horizon):
        print( " t = {} ".format(t).center(64, "-") )

        print( "jz = ", jz.t() )
        ja, logp, hiddens = policy(jz, hiddens)

        print( "ja = ", ja.t() )
        print( "logp = ", logp.t() )

        jz, r = dpomdp.execute(ja.detach())

        print("r = ", r.t() )
    print()

    print('-'*64)
    print("Test forward with act-obs".center(64))
    print('-'*64)

    policy = RNNPolicy(dpomdp.n_joint_observations * dpomdp.n_joint_actions, dpomdp.n_joint_actions)

    dpomdp.reset(args.batch)
    jaz = policy.init_inputs(args.batch)
    hiddens = policy.init_hiddens(args.batch)

    for t in range(args.horizon):
        print( " t = {} ".format(t).center(64, "-") )

        print( "jz = ", jz.t() )
        ja, logp, hiddens = policy(jaz, hiddens)

        print( "ja = ", ja.t() )
        print( "logp = ", logp.t() )

        ja = ja.detach()
        jz, r = dpomdp.execute(ja)
        jaz = ja * dpomdp.n_joint_observations + jz

        print("r = ", r.t() )
    print()
