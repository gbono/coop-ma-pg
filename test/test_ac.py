#!/usr/bin/env python3

from argparse import ArgumentParser

import torch

from model import DecPOMDP
from rl.deep import RNNActorCritic

HORIZON = 10
BATCH = 10

def get_args():
    parser = ArgumentParser()
    parser.add_argument("filename", type = str)
    parser.add_argument("--horizon", "-p", type = int, default = HORIZON)
    parser.add_argument("--batch", "-b", type = int, default = BATCH)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()

    dpomdp = DecPOMDP.parse(args.filename)

    print('-'*64)
    print("Test constructor".center(64))
    print('-'*64)
    ac = RNNActorCritic(dpomdp.n_joint_observations, dpomdp.n_joint_actions)
    print(ac)
    print()

    dpomdp.reset(args.batch)
    jz = ac.init_inputs(args.batch)
    hiddens = ac.init_hiddens(args.batch)

    print('-'*64)
    print("Test forward".center(64))
    print('-'*64)

    for t in range(args.horizon):
        print( " t = {} ".format(t).center(64, "-") )

        print( "jz = ", jz.t() )
        ja, logp, val, hiddens = ac(jz, hiddens)

        print( "ja = ", ja.t() )
        print( "logp = ", logp.t() )
        print( "val = ", val.t() )

        jz, r = dpomdp.execute(ja.detach())

        print("r = ", r.t() )
    print()

    print('-'*64)
    print("Test forward with act-obs".center(64))
    print('-'*64)

    ac = RNNActorCritic(dpomdp.n_joint_observations * dpomdp.n_joint_actions, dpomdp.n_joint_actions)

    dpomdp.reset(args.batch)
    jaz = ac.init_inputs(args.batch)
    hiddens = ac.init_hiddens(args.batch)

    for t in range(args.horizon):
        print( " t = {} ".format(t).center(64, "-") )

        print( "jz = ", jz.t() )
        ja, logp, val, hiddens = ac(jaz, hiddens)

        print( "ja = ", ja.t() )
        print( "logp = ", logp.t() )
        print( "val = ", val.t() )

        ja = ja.detach()
        jz, r = dpomdp.execute(ja)
        jaz = ja * dpomdp.n_joint_observations + jz

        print("r = ", r.t() )
    print()
