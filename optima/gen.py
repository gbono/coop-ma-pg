import os
import csv

raw = """
dectiger         2   -4
dectiger         3   5.1908
dectiger         4   4.8027
dectiger         5   7.0264
dectiger         6   10.381
dectiger         7   9.9935
dectiger         8   12.217
dectiger         9   15.572
dectiger         10  15.184
dectiger         inf 13.448

recycling        2   7
recycling        3   10.66
recycling        4   13.38
recycling        5   16.486
recycling        10  31.863
recycling        30  93.402
recycling        70  216.47
recycling        100 308.78
recycling        inf 31.929

Grid3x3corners   2   0
Grid3x3corners   3   0.133
Grid3x3corners   4   0.432
Grid3x3corners   5   0.894
Grid3x3corners   6   1.491
Grid3x3corners   7   2.19
Grid3x3corners   8   2.96
Grid3x3corners   9   3.80
Grid3x3corners   10  4.68
Grid3x3corners   20  14.35
Grid3x3corners   30  24.33
Grid3x3corners   inf 5.802

broadcastChannel 2   2
broadcastChannel 3   2.99
broadcastChannel 4   3.89
broadcastChannel 5   4.79
broadcastChannel 10  9.29
broadcastChannel 30  27.42
broadcastChannel 50  45.50
broadcastChannel 100 90.76
broadcastChannel inf 9.271

GridSmall        2   0.37
GridSmall        3   0.91
GridSmall        4   1.55
GridSmall        5   2.24
GridSmall        6   2.97
GridSmall        7   3.71
GridSmall        8   4.47
GridSmall        9   5.23
GridSmall        10  6.03

boxPushingUAI07  2   17.6
boxPushingUAI07  3   66.081
boxPushingUAI07  4   98.593
boxPushingUAI07  5   107.72
boxPushingUAI07  6   120.67
boxPushingUAI07  7   156.42
boxPushingUAI07  8   191.22
boxPushingUAI07  9   210.27
boxPushingUAI07  10  223.74
boxPushingUAI07  inf 224.43

Mars             2   5.8
Mars             3   9.38
Mars             4   10.18
Mars             5   13.26
Mars             6   18.62
Mars             7   20.90
Mars             8   22.47
Mars             9   24.31
Mars             10  26.31
Mars             inf 26.94
"""

for line in raw.splitlines():
    if not line:
        continue
    b, p, v = line.split()
    if not os.path.isdir(b):
        os.makedirs(b)
    with open(os.path.join(b, "p{}_ep_value.csv".format(p)), 'w') as f:
        writer = csv.DictWriter(f, fieldnames = ["episode", "mean", "min", "max"])
        writer.writeheader()
        writer.writerow({"episode":0, "mean":v, "min":v, "max":v})

