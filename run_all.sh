for bench in $(find ./preload -type f)
do
	for hrz in 2 3 5 10
	do
		for algo in $(find ./algorithms -type f -executable)
		do
			if [[ $algo = *"actor_critic"* ]]
			then
				cmd="$algo $bench -d 1.0 -p $hrz -t 3 -e 10000 -b 128 -l 0.01 -y 0.96 -s 2000 --error-type td -o td_p$hrz -c 20"
				echo $cmd
				$cmd
			fi
			cmd="$algo $bench -d 1.0 -p $hrz -t 3 -e 10000 -b 128 -l 0.01 -y 0.96 -s 2000 --error-type mc -o mc_p$hrz -c 20"
			echo $cmd
			$cmd
		done
	done
done
